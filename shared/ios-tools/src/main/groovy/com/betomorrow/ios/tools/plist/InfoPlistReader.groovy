package com.betomorrow.ios.tools.plist

interface InfoPlistReader {

    InfoPlist read(String source);

}