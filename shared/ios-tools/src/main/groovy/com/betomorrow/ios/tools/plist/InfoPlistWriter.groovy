package com.betomorrow.ios.tools.plist

interface InfoPlistWriter {

    void write(InfoPlist plist, String destination);

}