package com.betomorrow.ios.tools.plist

import groovy.transform.Canonical

@Canonical
class InfoPlist {

    String bundleShortVersion;
    String bundleVersion;
    String bundleIdentifier;

}
