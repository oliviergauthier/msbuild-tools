package com.betomorrow.android.tools.manifest

interface AndroidManifestWriter {

    void write(AndroidManifest manifest, String destination);
}
