package com.betomorrow.android.tools.manifest

interface AndroidManifestReader {

    AndroidManifest read(String source)

}