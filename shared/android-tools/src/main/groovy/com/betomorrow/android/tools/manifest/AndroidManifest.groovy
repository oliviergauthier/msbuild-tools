package com.betomorrow.android.tools.manifest

import groovy.transform.Canonical

@Canonical
class AndroidManifest {

    String versionCode;
    String versionName;
    String packageName;

}
