package com.betomorrow.msbuild.tools.Files

interface FileCopier {

    void replace(String src, String dst);

}
